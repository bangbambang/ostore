# README #

For task 1 (online store)

### Problem Statement ###

When we look at ratings for our online store application, we received the following
facts:

- Customers were able to put items in their cart, check out, and then pay. After several days, many of our customers received calls from our Customer Service department stating that their orders have been canceled due to stock unavailability.
- These bad reviews generally come within a week after our 12.12 event, in which we held a large flash sale and set up other major discounts to promote our store.

After checking in with our Customer Service and Order Processing departments, we received the following additional facts:

- Our inventory quantities are often misreported, and some items even go as far as having a negative inventory quantity.
- The misreported items are those that performed very well on our 12.12 event.
- Because of these misreported inventory quantities, the Order Processing department was unable to fulfill a lot of orders, and thus requested help from our Customer Service department to call our customers and notify them that we have had to cancel their orders.

#### Requirements ####

- Analyse and describe the root cause and proposed solutions for said problem
- Build a PoC

## Problem Analysis ##

Based on mentioned facts, most likely probable cause is order flow control and verification. Detail and possible solutions for the problem is as follow:

- It seems that the software did not verify stock availability before issuing sales order or fulfillment order.
This could be fixed by adding verification during checkout process to ensure that inventory quantity never goes below zero.
- Having users arbitrarily denied purchase probably will cause dissatisfaction. To deal with this problem, we could add optional parameter in items to limit sale amount. This will give additional benefits of having a healthy inventory and predictable cost for recurring promotional events.

## Solution Description and Constraints ##

### Proposed Solution ###

- Flash sales must have a predefined items list and specific amount of available quantity per item.
- Discount will not have such limit. But failed checkout will be logged and customer could opt-in for a reminder if and when the desired item(s) is available again.
- To lighten the system load, real-time verification will be done during checkout. The aggregate amount for each flash-sale event will be updated every n-seconds (tunable).

### PoC Constraints ###

Due to limitation of info, there are some constraints given to the PoC implementation:

- Floating inventory are not considered in available stock count. This includes return items in transit, ordered item that haven't been paid for by customers, and purchased stock that haven't been received yet.
- PoC will only deal with inventory dynamics from sales and fulfillment perspective, there will be no customer-side "frills" such as auth mechanism, shopping chart, and payment checking.